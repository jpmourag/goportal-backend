FROM node:18.16.0-bullseye

# Install required dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        libc6 \
        libstdc++6 \
        libsqlite3-dev \
        build-essential \
    && rm -rf /var/lib/apt/lists/*

RUN apt autoremove

WORKDIR /app

COPY package*.json ./

RUN npm install -g npm@latest

RUN npm install

RUN npm rebuild bcrypt --build-from-source

COPY . .

RUN npm run build

EXPOSE 3333

ENTRYPOINT ["npm", "run", "start"]
